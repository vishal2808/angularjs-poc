package controllers;

import play.mvc.*;
import views.html.index;
import views.html.admin;

public class Application extends Controller {

	public Result index() {
	    return ok(index.render(null));
    }

	public Result admin() {
		return ok(admin.render(null));
	}

}
