angular.module("sportsStoreAdmin", [ "ngRoute" ]).config(
			function($routeProvider) {
				$routeProvider.when("/login", {
					templateUrl : "/html/views/adminLogin.html"
				});
				$routeProvider.when("/main", {
					templateUrl : "/html/views/adminMain.html"
				});
				$routeProvider.otherwise({
					redirectTo : "/login"
				});
			});