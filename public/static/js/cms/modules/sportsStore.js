angular.module("sportsStore", [ "customFilters", "cart", "ngRoute" ]).config(
		function($routeProvider) {
			$routeProvider.when("/complete", {
				templateUrl : "/html/views/thankYou.html"
			});
			$routeProvider.when("/placeorder", {
				templateUrl : "/html/views/placeOrder.html"
			});
			$routeProvider.when("/checkout", {
				templateUrl : "/html/views/checkoutSummary.html"
			});
			$routeProvider.when("/products", {
				templateUrl : "/html/views/productList.html"
			});
			$routeProvider.otherwise({
				templateUrl : "/html/views/productList.html"
			});
		}).constant("dataUrl", "http://localhost:2403/products").controller(
		"sportsStoreCtrl", function($scope, $http, dataUrl) {
			$scope.data = {};
			$scope.pageTitle = 'Title of the page';
			$http.get(dataUrl).success(function(data) {
				$scope.data.products = data;
			}).error(function(error) {
				$scope.data.error = error;
			});
		});